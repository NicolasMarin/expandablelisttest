package com.pds.expandablelisttest;

import java.util.List;

public class Item {
    String name;
    List<SubItem> subItems;

    public Item(String name, List<SubItem> subItems) {
        this.name = name;
        this.subItems = subItems;
    }
}
