package com.pds.expandablelisttest

import android.os.Bundle
import android.widget.Button
import android.widget.ExpandableListView
import androidx.appcompat.app.AppCompatActivity


class MainActivity : AppCompatActivity() {

    var expandableListAdapter : CustomExpandableListAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var amount = 0
        val expandableListView = findViewById<ExpandableListView>(R.id.list)
        expandableListAdapter = CustomExpandableListAdapter(this/*, expandableListTitle, expandableListDetail*/)
        expandableListView.setAdapter(expandableListAdapter)

        val btnAdd = findViewById<Button>(R.id.add).setOnClickListener {
            updateAdapterList(getListWithNewItem(amount))
            expandableListView.expandGroup(amount)
            amount++
        }

        val btnRemove = findViewById<Button>(R.id.remove).setOnClickListener {
            val list = if (expandableListAdapter!!.items != null) expandableListAdapter!!.items else mutableListOf()
            if (list.isNotEmpty()) {
                list.removeLast()
                updateAdapterList(list)
                amount--
            }
        }
    }

    private fun getListWithNewItem(amount: Int): List<Item> {
        val expandableListDetail = ExpandableListDataPump.getData(amount)
        val list = if (expandableListAdapter!!.items != null) expandableListAdapter!!.items else mutableListOf()
        list.add(expandableListDetail)
        return list
    }

    private fun updateAdapterList(list: List<Item>) {
        expandableListAdapter!!.load(list)
        expandableListAdapter!!.notifyDataSetChanged()
    }

    private fun getDistance(list: MutableList<Item>): Int {
        var count = 0
        for (item in list) {
            count++
            for (subItem in item.subItems) {
                count++
            }
        }
        return count
    }
}