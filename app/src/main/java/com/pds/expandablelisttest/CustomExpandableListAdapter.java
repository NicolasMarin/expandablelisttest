package com.pds.expandablelisttest;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.TextView;

import java.util.List;

public class CustomExpandableListAdapter extends BaseExpandableListAdapter {

    private Context context;
    private List<Item> items;
    ExpandableListView mExpandableListView;

    public CustomExpandableListAdapter(Context context) {
        this.context = context;
    }

    @Override
    public Object getChild(int listPosition, int expandedListPosition) {
        return items.get(listPosition).subItems.get(expandedListPosition);
    }

    @Override
    public long getChildId(int listPosition, int expandedListPosition) {
        return expandedListPosition;
    }

    @Override
    public View getChildView(int listPosition, final int expandedListPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {
        final SubItem expandedListText = (SubItem) getChild(listPosition, expandedListPosition);
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.list_item, null);
        }
        TextView expandedListTextView = convertView.findViewById(R.id.expandedListItem);
        expandedListTextView.setText(expandedListText.name);
        return convertView;
    }

    @Override
    public int getChildrenCount(int listPosition) {
        return items.get(listPosition).subItems.size();
    }

    @Override
    public Object getGroup(int listPosition) {
        return items.get(listPosition);
    }

    @Override
    public int getGroupCount() {
        return items != null ? items.size() : 0;
    }

    @Override
    public long getGroupId(int listPosition) {
        return listPosition;
    }

    @Override
    public View getGroupView(int listPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        Item listTitle = (Item) getGroup(listPosition);
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.list_group, null);
        }
        if (mExpandableListView == null) {
            mExpandableListView = (ExpandableListView) parent;
        }
        TextView listTitleTextView = convertView.findViewById(R.id.listTitle);
        listTitleTextView.setTypeface(null, Typeface.BOLD);
        listTitleTextView.setText(listTitle.name);
        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int listPosition, int expandedListPosition) {
        return true;
    }

    public void load(List<Item> items) {
        this.items = items;
    }

    public List<Item> getItems() {
        return items;
    }

    @Override
    public void onGroupExpanded(int groupPosition) {
        super.onGroupExpanded(groupPosition);
        handleScrolling(groupPosition);
    }

    private void handleScrolling(int groupPosition) {
        if (mExpandableListView != null) {
            mExpandableListView.setTranscriptMode(groupPosition == items.size()-1 ?
                    ExpandableListView.TRANSCRIPT_MODE_ALWAYS_SCROLL : ExpandableListView.TRANSCRIPT_MODE_DISABLED);
        }
    }

    @Override
    public void onGroupCollapsed(int groupPosition) {
        super.onGroupCollapsed(groupPosition);
        handleScrolling(groupPosition);
    }
}