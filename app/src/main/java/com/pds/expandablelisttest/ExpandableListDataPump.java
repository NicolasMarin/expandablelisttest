package com.pds.expandablelisttest;

import java.util.ArrayList;
import java.util.List;

public class ExpandableListDataPump {
    public static Item getData(int amount) {
        List<SubItem> cricket = new ArrayList<>();
        cricket.add(new SubItem("India"));
        cricket.add(new SubItem("Pakistan"));
        cricket.add(new SubItem("Australia"));
        cricket.add(new SubItem("England"));
        cricket.add(new SubItem("South Africa"));

        return new Item(amount + " CRICKET TEAMS", cricket);
    }
}
